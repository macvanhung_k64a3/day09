<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cookie</title>
</head>
<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.main {
    font-size: 20px;
    background-color: rgb(255,255,255);
    padding: 128px;
}


.main .wrap {
    background-color: #fff;
    padding-top: 48px;
    border-radius: 32px;

}

.wrap .heading {
    text-align: center;
    margin-top: 32px;
}


.wrap .quest-wrap {
    padding: 32px 64px;
}

.wrap #result {
    height: 60vh;
    font-size: 32px;
    text-align: center;
    font-weight: 800;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
}

#result .comment {
    margin-top: 24px;
}

#result .btn-retry {
    padding: 8px;
    background-color: #5fe85a;
    font-size: 20px;
    color: #fff;
    text-decoration: none;
    border-radius: 6px;
    margin-top: 24px;
}

.quest-wrap .page-1,
.quest-wrap .page-2 {
    margin-top: 32px;
    transition: all 1s ease-in;
}


.page-1 .next {
    display: flex;
    align-items: center;
    justify-content: end;
    /* width: 90px; */
    cursor: pointer;
    margin-top: 32px;

}

.page-1 .next:hover span {
    opacity: 0.9;
}

.next span {
    background-color: #5fe85a;
    color: #fff;
    font-size: 20px;
    padding: 12px;
    border-radius: 4px;
}

.page .quest {
    margin: 24px 0;
}

.quest .ask {
    font-weight: 600;
}

.ask .ask-text {
    font-weight: 600;

}

.quest .answer {
    margin-top: 16px;
}

.answer .input-group {
    margin-top: 8px;
}

.page .page-btn {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;

}

.page-btn>div {
    padding: 12px;
    font-size: 20px;
    text-align: center;
    max-width: 200px;
    border-radius: 6px;
    margin-top: 32px;
    /* margin: 0 128px; */
}

.page-btn>div:hover {
    cursor: pointer;
    opacity: 0.9;
}

.page-btn .submit-btn {
    background-color: #5fe85a;
    color: #fff;
}

.page-btn .back {
    background-color: #e63d50;
    color: #fff;
}

</style>

<body>
    <div class="main">
        <div class="wrap">
            <h2 class="heading">Trắc nghiệm</h2>
            <div class="quest-wrap">
                <div class="page page-1">
                    <div class="quest-list">

                    </div>
                    <div class="next"><span>Next</span></div>
                </div>
                <div class="page page-2" style="display: none;">
                    <div class="quest-list">

                    </div>
                    <div class="page-btn">
                        <div class="back">Back</div>
                        <div class="submit-btn">Nộp bài</div>
                    </div>
                </div>
            </div>
            <div id="result" style="display:none">
            </div>
        </div>

    </div>
    <script>
        const quiz = [{
                quest: "Có mấy tác dụng thường thấy trong lựa chọn trật từ trong câu?",
                choices: [
                    "Một",
                    "Hai",
                    "Ba",
                    "Bốn",
                ],
                answer: "4"
            },
            {
                quest: "Chức năng chính của câu trần thuật là gì?",
                choices: [
                    "Dùng để bộc lộ trực tiếp cảm xúc của người nói",
                    "Dùng để hỏi",
                    "Dùng để ra lệnh, yêu cầu, đề nghị, khuyên bảo, ...",
                    "Dùng để kể, thông báo, nhận định, trình bày, miêu tả, ...",
                ],
                answer: "4"
            },
            {
                quest: "Chức năng chính của câu cầu khiến là gì?",
                choices: [
                    "Dùng để bộc lộ trực tiếp cảm xúc của người nói.",
                    "Dùng để hỏi",
                    "Dùng để ra lệnh, yêu cầu, đề nghị, khuyên bảo, ...",
                    "Dùng để kể, thông báo, nhận định, trình bày, miêu tả, ...",
                ],
                answer: "3"
            },
            {
                quest: "Câu nào không phải câu phủ định trong các câu sau?",
                choices: [
                    "Em chưa học bài.",
                    "Em chẳng ăn cơm.",
                    "Không phải em không học bài.",
                    "Em không đi chơi nữa.",
                ],
                answer: "3"
            },
            {
                quest: "Chức năng chính của câu nghi vấn là gì?",
                choices: [
                    "Dùng để bộc lộ trực tiếp cảm xúc của người nói.",
                    "Dùng để hỏi",
                    "Dùng để ra lệnh, yêu cầu, đề nghị, khuyên bảo, ...",
                    "Dùng để kể, thông báo, nhận định, trình bày, miêu tả, ...",
                ],
                answer: "2"
            },

            {
                quest: "Trong các câu sau, câu nào không phải câu trần thuật?",
                choices: [
                    " Lúc bấy giờ ta cùng các ngươi sẽ bị bắt, đau xót biết chừng nào!",
                    "Thạch Sanh thật thà tin ngay.",
                    "Đêm nay đến phiên anh canh miếu thờ, ngặt vì cất dở mẻ rượi, em chịu khó thay anh, đến sáng thì về.",
                    "Cả 3 đều sai.",
                ],
                answer: "1"
            },
            {
                quest: "Chức năng chính của câu cảm thán là gì?",
                choices: [
                    "Dùng để bộc lộ trực tiếp cảm xúc của người nói.",
                    "Dùng để hỏi",
                    "Dùng để ra lệnh, yêu cầu, đề nghị, khuyên bảo, ...",
                    "Dùng để kể, thông báo, nhận định, trình bày, miêu tả, ...",
                ],
                answer: "1"
            },
            {
                quest: "Câu là câu trần thuật được dùng theo lối gián tiếp?",
                choices: [
                    "Thạch Sanh lại thật thà tin ngay.",
                    "Đêm nay, đến phiên anh canh miếu thờ, ngặt vì cất dở mẻ rượu, em chịu khó thay anh, đến sáng thì về.",
                    "Thế là Sọ Dừa đến ở nhà phú ông.",
                    "Từ đó, nhuệ khí của nghĩa quân ngày một tăng.",
                ],
                answer: "2"
            },
            {
                quest: "Câu nào dưới đây không thể hiện hành động hứa hẹn?",
                choices: [
                    "Con sẽ chăm chỉ học bài hơn nữa.",
                    "Chúng tôi nguyện đem xương thịt của mình theo minh công, cùng với thanh gươm thần này để báo đền Tổ quốc!",
                    "Họ đang quyết tâm hoàn thành công việc trong thời gian ngắn nhất.",
                    "Chúng em xin hứa sẽ đạt kết quả cao trong kì thi này.",
                ],
                answer: "3"
            },
            {
                quest: "Trật tự từ của câu nào nhấn mạnh sự vật?",
                choices: [
                    "Quê hương anh nươc mặn đồng chua. (Chính Hữu)",
                    "Sáng chớm lạnh trong lòng Hà Nội. (Nguyễn Đình Thi)",
                    "Gian nan đời vẫn ca vang núi đèo. (Tố Hữu)",
                    "Chiến trường đi chẳng tiếc đời xanh. (Quang Dũng)",
                ],
                answer: "3"
            },

        ]
        renderquest(quiz);

        var nextPageBtn = document.querySelector('.next');
        var backBtn = document.querySelector('.back');
        var submitBtn = document.querySelector('.submit-btn');
        var page1 = document.querySelector('.page-1');
        var page2 = document.querySelector('.page-2');
        var answers = document.getElementsByName("answer")
        var answerForms = document.querySelectorAll('.answerForm');
        var result = document.getElementById('result');
        var diem = 0;
        console.log(4433211233)

        function checkResult(diem) {
            if (diem < 4) {
                result.innerHTML += '<span class="comment" style="color:#dc3545 ;">' + "Bạn quá kém, cần ôn tập thêm" + '</span>';
            } else if (diem >= 4 && diem <= 7) {
                result.innerHTML += '<span class="comment" style="color: #ffc107;">' + "Cũng bình thường" + '</span>';
            } else if (diem > 7) {
                result.innerHTML += '<span class="comment" style="color:#28a745 ;">' + "Sắp sửa làm được trợ giảng lớp PHP" + '</span>';
            }
        }
        answerForms.forEach((answerForm, index) => {
            let answer = answerForm.elements.length

            answerForm.onchange = function() {
                for (let i = 0; i < answerForm.elements.length; i++) {
                    const element = answerForm.elements[i];
                    var userChoice = Number(element.value) + 1;

                    if (element.checked == true) {
                        if (userChoice === Number(quiz[index].answer)) {
                            diem++;
                            console.log(diem)
                        } else {
                            console.log('Wrong answer')
                        }
                    } else {}

                }
            }
        })
        function renderPage(i, data, index) {
            var page = document.querySelector(`.page-${i} .quest-list`);
            var ask = ` <div class="ask">
                            <p class="ask-text">
                                <span class="quest-order">Câu ${index+1}: ${data.quest }</span>
                                
                            </p>
                        </div>`
            var answers = ``
            data.choices.forEach((choice, num) => {
                answers += `<div class="input-group">
                                    <input type="radio" name="answer" value="${num}" id="answer_1">
                                    <label class="form-label">${choice}</label>
                                </div>`
            })
            page.innerHTML += `<div class="quest">` +
                ask +
                `<div class="answer">
                                        <form action="" class="answerForm">` +
                answers +
                `</form>
                                    </div>
                                </div>`

        }

        function renderquest(quiz) {
            quiz.forEach((quest, index) => {

                if (index <= 4) {
                    renderPage(1, quest, index)
                } else renderPage(2, quest, index)

            })
        }

        submitBtn.onclick = function() {
            page1.style.display = 'none';
            page2.style.display = 'none';
            result.style.display = 'flex'
            result.innerHTML = "Điểm : " + diem + "/" + quiz.length + "</br>";
            checkResult(diem)
        }


        nextPageBtn.onclick = function() {
            page1.style.display = 'none';
            page2.style.display = 'block';
            console.log('to page 2')
        }
        backBtn.onclick = function() {
            page2.style.display = 'none';
            page1.style.display = 'block';
        }
    </script>
</body>

</html>